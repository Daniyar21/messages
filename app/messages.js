const express = require('express');
const fs = require('fs');
const router = express.Router();
const {readdir} = require('fs/promises')

router.post('/', (req, res) => {
  const date = new Date();
  const dateTime = date.toISOString();

  const filename = `./messages/${dateTime}.txt`;
  const response = JSON.stringify({message: req.body.message, datetime: dateTime});

  fs.writeFileSync(filename, response);
  res.send(response);
});


const  path = './messages';

router.get('/', async (req, res) => {

  const allMessages = [];
  const files = await readdir(path);
  console.log(files);
   const messages = files.forEach(f=>{
     const message = fs.readFileSync(`./messages/${f}`);
     allMessages.push(JSON.parse(message));
   })

  res.send(allMessages);
});


module.exports = router;